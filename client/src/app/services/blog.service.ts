import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class BlogService {

  options;

  constructor(private authService: AuthService,
              private http: Http) { }

  createAuthenticationHeaders() {
    this.authService.loadToken();
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'authorization': this.authService.authToken
      })
    });
  }

  newBlog(blog) {
    this.createAuthenticationHeaders();
    return this.http.post('blogs/newBlog', blog, this.options)
    .map(res => res.json());
  }

  getAllBlogs() {
    this.createAuthenticationHeaders();
    return this.http.get('blogs/allBlogs', this.options)
    .map(res => res.json());
  }

  getSingleBlog(id) {
    this.createAuthenticationHeaders();
    return this.http.get('blogs/singleBlog/' + id, this.options)
    .map(res => res.json());
  }

  editBlog(blog) {
    this.createAuthenticationHeaders(); // Create headers
    return this.http.put('blogs/updateBlog/', blog, this.options).map(res => res.json());
  }

  deleteBlog(id) {
    this.createAuthenticationHeaders(); // Create headers
    return this.http.delete('blogs/deleteBlog/' + id, this.options).map(res => res.json());
  }

  likeBlog(id) {
    const info = { id: id };
    return this.http.put('blogs/likeBlog/', info, this.options).map(res => res.json());
  }

  dislikeBlog(id) {
    const info = { id: id };
    return this.http.put('blogs/dislikeBlog/', info, this.options).map(res => res.json());
  }
}
