import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

  constructor() { }

  validateRegister(user) {
    if(user.username == undefined || user.email == undefined || user.password == undefined ) {
      return false;
    } else {
      return true;
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateTitle(title) {
    if(title.length < 5 || title.length > 20) {
      return false;
    } else {
      return true;
    }
  }

  validateBody(body) {
    if(body.length < 5 || body.length > 20) {
      return false;
    } else {
      return true;
    }
  }
}
