import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {  ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AccessibleProfileComponent } from './components/accessible-profile/accessible-profile.component';
import { BlogComponent } from './components/blog/blog.component';

import { FlashMessagesModule } from 'angular2-flash-messages';

//Services
import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { BlogService } from './services/blog.service';
//Guard for links
import { AuthGuard } from './guards/auth.guard';
import { LoggedInGuard } from './guards/logged-in.guard';
import { BannedGuard } from './guards/banned.guard';
import { AdminGuard } from './guards/admin.guard';

import { UpdatePostComponent } from './components/update-post/update-post.component';
import { DeletePostComponent } from './components/delete-post/delete-post.component';
import { FileSelectDirective } from 'ng2-file-upload';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'register', component: RegisterComponent, canActivate: [LoggedInGuard]},
  {path: 'login', component: LoginComponent, canActivate: [LoggedInGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'blog', component: BlogComponent, canActivate: [BannedGuard, AuthGuard]},
  {path: 'edit-blog/:id', component: UpdatePostComponent, canActivate: [AuthGuard, BannedGuard]},
  {path: 'delete-blog/:id', component: DeletePostComponent, canActivate: [AuthGuard, BannedGuard]},
  {path: 'user/:username', component: AccessibleProfileComponent, canActivate: [AuthGuard, AdminGuard]}
]


@NgModule({ 
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BlogComponent,
    UpdatePostComponent,
    DeletePostComponent,
    UpdatePostComponent,
    DeletePostComponent,
    AccessibleProfileComponent,
    FileSelectDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,  
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot()
  ],
  providers: [ValidateService, AuthService, AuthGuard, BannedGuard, AdminGuard, LoggedInGuard, BlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
