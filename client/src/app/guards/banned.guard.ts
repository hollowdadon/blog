import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Injectable()
export class BannedGuard implements CanActivate{
    constructor(private authService: AuthService,
                private router: Router,
                private flashMessage: FlashMessagesService) { }

    isBanned: Boolean;

    canActivate() {
        this.authService.getProfile().subscribe(profile => {
            this.isBanned = profile.user.isBanned;
        });
        console.log(this.isBanned + " Banned guard");
        if(!this.isBanned) {
            return true;
        } else {
            this.flashMessage.show('Your are banned on this website', {cssClass: 'alert-danger', timeout: 1000});
            this.router.navigate(['/profile']);
            return false;
        }
    }
}