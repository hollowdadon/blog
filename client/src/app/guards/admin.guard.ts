import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Injectable()
export class AdminGuard implements CanActivate{
    constructor(private authService: AuthService,
                private router: Router,
                private flashMessage: FlashMessagesService) { }

    role: String;

    canActivate() {
        this.authService.getProfile().subscribe(profile => {
            this.role = profile.user.role;
        });
        if(this.role == "user") {
            //return true;
            this.flashMessage.show('Your are not admin on this website', {cssClass: 'alert-danger', timeout: 1000});
            this.router.navigate(['/blog']);
            return false;
        } else {
            return true;
        }
    }
}