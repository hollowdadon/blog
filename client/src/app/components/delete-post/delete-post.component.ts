import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-delete-post',
  templateUrl: './delete-post.component.html',
  styleUrls: ['./delete-post.component.css']
})
export class DeletePostComponent implements OnInit {

  message;
  messageClass;
  foundBlog = false;
  processing = false;
  blog: any = {};
  currentUrl;

  constructor(private blogService: BlogService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  deleteBlog() {
    this.processing = true;
    this.blogService.deleteBlog(this.currentUrl.id).subscribe(data => {
      if(!data.success) {
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        setTimeout(() => {
          this.router.navigate(['/blog']);
        }, 1000);
      }
    });
  }

  ngOnInit() {
    this.currentUrl = this.activatedRoute.snapshot.params;
    this.blogService.getSingleBlog(this.currentUrl.id).subscribe(data => {
      if(!data.success) {
        this.messageClass = 'alert alert danger';
        this.message = data.message;
      } else {
        this.blog = {
          title: data.blog.title,
          body: data.blog.body,
          owner: data.blog.owner,
          dateOfCreating: data.blog.dateOfCreating
        }
        this.foundBlog = true;
      }
    });
  }
}
