import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, ReactiveFormsModule, ValidatorFn  } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { BlogService } from '../../services/blog.service';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  newBlog = false;
  loadingBlogs = false;
  form;
  processing = false;
  username;
  blogPosts;
  comment = [];
  commentForm;
  avatar;
  isBanned: Boolean;
  role: String;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private blogService: BlogService,
              private flashMessage: FlashMessagesService)
   {
     this.createNewBlogForm();
     this.newComment();
    }

  createNewBlogForm() {
    this.form = this.formBuilder.group({
      'title': ['', [Validators.required, Validators.maxLength(50), Validators.minLength(5)]],
      'body': ['', [Validators.required, Validators.maxLength(500), Validators.minLength(5)]]
    });
  }

  enableNewBlogForm() {
    this.form.get('title').enable();
    this.form.get('body').enable();
  }


  disableNewBlogForm() {
    this.form.get('title').disable();
    this.form.get('body').disable();
  }

  newBlogForm() {
    this.newBlog = true;
    
  }

  reloadBlogs() {
    this.loadingBlogs = true;
    this.displayAllBlogs();
    setTimeout(() => {
      this.loadingBlogs = false;
    }, 3000);
  }

  onBlogSubmit() {
    this.processing = true;
    this.disableNewBlogForm();

    const blog = {
      title: this.form.get('title').value,
      body: this.form.get('body').value,
      owner: this.username
    }

    this.blogService.newBlog(blog).subscribe(data => {
      if(!data.success) {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 5000});
        this.processing = false;
        this.enableNewBlogForm();
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-success', timeout: 5000});
        this.displayAllBlogs();
        setTimeout(() => {
          this.newBlog = false;
          this.processing = false;
          this.form.reset();
          this.enableNewBlogForm();
        }, 2000)
      }
    });
  }

  refreshForm(){
    this.form.reset();
  }

  goBack() {
    window.location.reload();
  }

  displayAllBlogs() {
    this.blogService.getAllBlogs().subscribe(data => {
      this.blogPosts = data.blogs;
    });
  }

  like(id) {
    this.blogService.likeBlog(id).subscribe(data => {
      this.displayAllBlogs();
    });
  }

  dislike(id) {
    this.blogService.dislikeBlog(id).subscribe(data => {
      this.displayAllBlogs();
    });
  }

  addComment(id) {
    this.comment = [];
    this.comment.push(id);
  }

  newComment() {
    this.commentForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.maxLength(100), Validators.minLength(3)]]
    })
  }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.username = profile.user.username;
      this.isBanned = profile.user.isBanned;
      this.role = profile.user.role;
      this.avatar = profile.user.avatar;
      this.displayAllBlogs();
    });
  }

}
