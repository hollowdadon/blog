import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  username: String;
  email: String;
  password: String; 
  validation: String;

  constructor(private validateService: ValidateService,
              private flashMessage: FlashMessagesService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  }

  onRegisterSubmit() {
    const user = {
      username: this.username,
      email: this.email,
      password: this.password,
      validation: this.validation
    }

    //Required Fiedls
    if(!this.validateService.validateRegister(user)) {
      this.flashMessage.show('Plese fill in all fields', {cssClass: 'alert-danger flash-message', timeout: 5000});
      return false;
    }

    //ValidateEmil
    if(!this.validateService.validateEmail(user.email)) {
      this.flashMessage.show('Type email correctly', {cssClass: 'alert-danger', timeout: 5000});
      return false;
    }

    //Register User
    this.authService.registerUser(user).subscribe(data => {
      if(data.success) {
        this.flashMessage.show('You are now registered you can log in now', {cssClass: 'alert-success', timeout: 5000});
        this.router.navigate(['/login']);
      }else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 5000});
        this.router.navigate(['/register']);
      }
    });
  }

}
