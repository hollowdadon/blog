//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, ElementRef, Input } from '@angular/core';
//import the file-upload plugin
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
//import the native angular http and respone libraries
import { Http, Response } from '@angular/http';
//import the do function to be used with the http library.
import "rxjs/add/operator/do";
//import the map function to be used with the http library
import "rxjs/add/operator/map";
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators, ReactiveFormsModule, ValidatorFn  } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user:any = {};
  form;
  allUsers;

  //public uploader:FileUploader = new FileUploader({url: 'http://localhost:3000/users/profile', itemAlias: 'photo'});

  constructor(private http: Http, 
              private el: ElementRef,
              private authService: AuthService,
              private router: Router,
              private formBuilder: FormBuilder,
              private flashMessage: FlashMessagesService) 
              { }

  displayAllUsers() {
      this.authService.getAllUsers().subscribe(data => {
        this.allUsers = data.users;
      });
  }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user;
      console.log(this.user.avatar);
      if(this.user.role == 'admin') {
          this.displayAllUsers();
      }
    },
    err => {
      console.log(err);
      return false;
    });
  }

  upload() {
    //locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    //get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    //create a new fromdata instance
        let formData = new FormData();
    //check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) { // a file was selected
            //append the key name 'photo' with the first file in the element
                formData.append('photo', inputEl.files.item(0));
            //call the angular http method
            this.http
        //post the form data to the url defined above and map the response. Then subscribe //to initiate the post. if you don't subscribe, angular wont post.
                .put('users/profile', formData).map((res:Response) => res.json()).subscribe(
                //map the success function and alert the response
                 (success) => {
                  this.flashMessage.show('Image added', {cssClass: 'alert-success', timeout: 5000});
                  window.location.reload();
                  //this.router.navigate(['blog']);
                },
                (error) => alert(error));
          }
       }

       uploadImageByLink() {
        this.authService.uploadImage().subscribe(data => {
            this.flashMessage.show('Image uploaded', {cssClass: 'alert-success', timeout: 5000});
            window.location.reload();
          });
       }

}
