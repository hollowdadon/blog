import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;
  isBanned: Boolean;

  constructor(private authService: AuthService,
              private router: Router,
              private flashMessage: FlashMessagesService) { }

  ngOnInit() {
  }

  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password
    }

    //Auth service
    this.authService.authenticateUser(user).subscribe(data => {
      if(data.success) {
        this.authService.storeUserData(data.token, data.user);
        this.authService.getProfile().subscribe(profile => {
        this.isBanned = profile.user.isBanned;
          console.log(this.isBanned);
          if(this.isBanned) {
            this.flashMessage.show('Your are banned on this website', {cssClass: 'alert-danger', timeout: 5000});
            this.router.navigate(['profile']);
          } else {
            this.flashMessage.show('Your login correctly', {cssClass: 'alert-success', timeout: 5000});
            this.router.navigate(['profile']);
          }
        });
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 5000});
        this.router.navigate(['login']);
      }
    });
  }

}
