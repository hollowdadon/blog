import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessibleProfileComponent } from './accessible-profile.component';

describe('AccessibleProfileComponent', () => {
  let component: AccessibleProfileComponent;
  let fixture: ComponentFixture<AccessibleProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessibleProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessibleProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
