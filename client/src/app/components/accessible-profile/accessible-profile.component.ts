import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-accessible-profile',
  templateUrl: './accessible-profile.component.html',
  styleUrls: ['./accessible-profile.component.css']
})
export class AccessibleProfileComponent implements OnInit {
  currentUrl;
  username;
  ownerUsername;
  ownerEmail;
  ownerAvatar;
  ownerIsBanned;
  keyword;
  role: String;

  constructor(private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.username = profile.user.username;
      this.role = profile.user.role;
      if(this.role == "admin") {
        this.currentUrl = this.activatedRoute.snapshot.params;
        this.authService.getAcessibleProfile(this.currentUrl.username).subscribe(data => {
          this.ownerUsername = data.user.username;
          this.ownerEmail = data.user.email;
          this.ownerAvatar = data.user.avatar;
          this.ownerIsBanned = data.user.isBanned;
          console.log(this.ownerAvatar);
        });
      } else {
         this.flashMessage.show('You are not admin on this website', {cssClass: 'alert-danger', timeout: 5000});
         this.router.navigate(['blog']);
      }
  
    });

  }

  banUser(username) {
    this.authService.banUser(this.ownerUsername).subscribe(data => {
      this.flashMessage.show('User is banned', {cssClass: 'alert-danger', timeout: 5000});
      window.location.reload();
    });
  }

  unbanUser(username) {
    this.authService.unbanUser(this.ownerUsername).subscribe(data => {
      this.flashMessage.show('User successfuly unbanned', {cssClass: 'alert-success', timeout: 5000});
      window.location.reload();
    });
  }

}
