const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');

const User = require('../models/user');
const Blog = require('../models/blog');

router.post('/newBlog', (req, res, next) => {
    if(!req.body.title) {
        res.json({success: false, message: 'Blog title is required'});
    } else {
        if(!req.body.title) {
            res.json({success: false, message: 'Blog body is required'});
        } else {
            if(!req.body.owner) {
                res.json({success: false, message: 'Blog owner is required'});
            } else {
                const blog = new Blog({
                    title: req.body.title,
                    body: req.body.body,
                    owner: req.body.owner
                });
                blog.save((err) => {
                    if(err) {
                        res.json({success: false, message: err});
                    } else {
                        res.json({success: true, message: 'Blog created correctly'});
                    }
                });
            }
        }
    }
});

router.get('/allBlogs', (req, res, next) => {
    Blog.find({}, (err, blogs) => {
        if(err) {
            res.json({success: false, message: err});
        } else {
            if(!blogs) {
                res.json({success: false, message: "No blogs found"});
            } else {
                res.json({success: true, blogs: blogs});
            }
        }
    }).sort({'likes': -1});   
});

router.get('/singleBlog/:id', (req, res) => {
    if(!req.params.id) {
        res.json({success: false, message: err});    
    } else {
        Blog.findOne({_id: req.params.id }, (err, blog) => {
            if(err) {
                res.json({success: false, message: err});  
            } else {
                if(!blog) {
                    res.json({success: false, message: 'Blog not found'});  
                } else {
                    User.findOne({ _id: localStorage.getItem('validatsia') }, (err, user) => {
                        if (err) {
                          res.json({ success: false, message: err });
                        } else {
                          if (!user) {
                            res.json({ success: false, message: 'Unable to authenticate user' }); 
                          } else {
                            if (user.username !== blog.owner) {
                              res.json({ success: false, message: 'You are not author of this post.' }); 
                            } else {
                              res.json({ success: true, blog: blog });
                            }
                          }
                        }
                      });
                }
            }
        });
    }  
});

router.put('/updateBlog', (req, res) => {
    if(!req.body._id) {
        res.json({success: false, message: err});  
    } else {
        Blog.findOne({_id: req.body._id}, (err, blog) => {
            if(err) {
                res.json({success: false, message: err});  
            } else {
                if(!blog) {
                    res.json({success: false, message: 'Blog id wasnt found'});  
                } else {
                    console.log(localStorage.getItem('validatsia'));
                    User.findOne({ _id: localStorage.getItem('validatsia') }, (err, user) => {
                        if (err) {
                          res.json({ success: false, message: err }); 
                        } else {
                          if (!user) {
                            res.json({ success: false, message: err}); 
                          } else {
                            if (user.username !== blog.owner) {
                              res.json({ success: false, message: 'You are not author of this post.' }); 
                            } else {
                              blog.title = req.body.title;
                              blog.body = req.body.body;
                              blog.save((err) => {
                                if (err) {
                                  if (err.errors) {
                                    res.json({ success: false, message: err});
                                  } else {
                                    res.json({ success: false, message: err });
                                  }
                                } else {
                                  res.json({ success: true, message: 'Blog Updated!' });
                                }
                              });
                            }
                          }
                        }
                      });
                }
            }
        });
    }
});



router.delete('/deleteBlog/:id', (req, res) => {
    if(!req.params.id) {
        res.json({success: false, message: err});  
    } else {
        Blog.findOne({_id: req.params.id}, (err, blog) => {
            if(err) {
                res.json({success: false, message: err});   
            } else {
                if(!blog) {
                    res.json({success: false, message: 'Blog wasnt found'});      
                } else {
                    User.findOne({_id: localStorage.getItem('validatsia')}, (err, user) => {
                        if(err) {
                            res.json({ success: false, message: err });
                        } else {
                            if(user.username !== blog.owner) {
                                res.json({ success: false, message: 'You are not author of this post.' }); 
                            } else {
                                blog.remove((err) => {
                                    if(err) {
                                        res.json({success: false, message: err}); 
                                    } else {
                                        res.json({success: true, message: 'Blog succesfully deleted.'});
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
});

router.put('/likeBlog', (req, res) => {
    if (!req.body.id) {
      res.json({ success: false, message: 'No id was provided.' }); 
    } else {
      Blog.findOne({ _id: req.body.id }, (err, blog) => {
        if (err) {
          res.json({ success: false, message: 'Invalid blog id' });
        } else {
          if (!blog) {
            res.json({ success: false, message: 'That blog was not found.' });
          } else {
            User.findOne({ _id: localStorage.getItem('validatsia') }, (err, user) => {
              if (err) {
                res.json({ success: false, message: 'Something went wrong.' });
              } else {
                if (!user) {
                  res.json({ success: false, message: 'Could not authenticate user.' });
                } else {
                  if (user.username === blog.owner) {
                    res.json({ success: false, messagse: 'Cannot like your own post.' });
                  } else {
                    if (blog.likedBy.includes(user.username)) {
                      res.json({ success: false, message: 'You already liked this post.' });
                    } else {
                      if (blog.dislikedBy.includes(user.username)) {
                        blog.dislikes--; 
                        const arrayIndex = blog.dislikedBy.indexOf(user.username); 
                        blog.dislikedBy.splice(arrayIndex, 1);
                        blog.likes++;
                        blog.likedBy.push(user.username);
                        blog.save((err) => {
                          if (err) {
                            res.json({ success: false, message: 'Something went wrong.' }); 
                          } else {
                            res.json({ success: true, message: 'Blog liked!' });
                          }
                        });
                      } else {
                        blog.likes++;
                        blog.likedBy.push(user.username); 
                        blog.save((err) => {
                          if (err) {
                            res.json({ success: false, message: 'Something went wrong.' }); 
                          } else {
                            res.json({ success: true, message: 'Blog liked!' });
                          }
                        });
                      }
                    }
                  }
                }
              }
            });
          }
        }
      });
    }
  });

  router.put('/dislikeBlog', (req, res) => {
    if (!req.body.id) {
      res.json({ success: false, message: 'No id was provided.' });
    } else {
      Blog.findOne({ _id: req.body.id }, (err, blog) => {
        if (err) {
          res.json({ success: false, message: 'Invalid blog id' });
        } else {
          if (!blog) {
            res.json({ success: false, message: 'That blog was not found.' }); 
          } else {
            User.findOne({ _id: localStorage.getItem('validatsia') }, (err, user) => {
              if (err) {
                res.json({ success: false, message: 'Something went wrong.' }); 
              } else {
                if (!user) {
                  res.json({ success: false, message: 'Could not authenticate user.' });
                } else {
                  if (user.username === blog.owner) {
                    res.json({ success: false, messagse: 'Cannot dislike your own post.' });
                  } else {
                    if (blog.dislikedBy.includes(user.username)) {
                      res.json({ success: false, message: 'You already disliked this post.' }); 
                    } else {
                      if (blog.likedBy.includes(user.username)) {
                        blog.likes--; 
                        const arrayIndex = blog.likedBy.indexOf(user.username);
                        blog.likedBy.splice(arrayIndex, 1);
                        blog.dislikes++; 
                        blog.dislikedBy.push(user.username);
                        blog.save((err) => {
                          if (err) {
                            res.json({ success: false, message: 'Something went wrong.' }); 
                          } else {
                            res.json({ success: true, message: 'Blog disliked!' });
                          }
                        });
                      } else {
                        blog.dislikes++; 
                        blog.dislikedBy.push(user.username); 
                        blog.save((err) => {
                          if (err) {
                            res.json({ success: false, message: 'Something went wrong.' }); 
                          } else {
                            res.json({ success: true, message: 'Blog disliked!' }); 
                          }
                        });
                      }
                    }
                  }
                }
              }
            });
          }
        }
      });
    }
  });

module.exports = router;