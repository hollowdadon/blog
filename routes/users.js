const express = require('express');
const router = express.Router();
const router2 = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const jsonWebToken = require('jsonwebtoken');
const config = require('../config/database');
const LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: 'angular-part/src/assets/images',
    filename: function(req, file, cb){
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
  });
const upload = multer({
    storage: storage
  }).single('photo');

const User = require('../models/user');

//Register route
router.post('/register', (req, res, next) => {
    let newUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        role: "user",
        isBanned: false
    });
    User.find({email: newUser.email}, (err, email) => {
        if(email.length) {
            res.json({success: false, message: 'User already exist'});
        } else {
            User.addUser(newUser, (err, user) => {
                if(err) {
                    res.json({success: false, message: 'Failed to register user'});
                } else {
                    res.json({success: true, message: 'User registred correctly'});
                }
            });
        }
    });

});

//Autheticate
router.post('/authenticate', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user) => {
        if(err) {
            throw err;
        }
        if(!user) {
            return res.json({ success: false, message: "User not found" });
        }
        User.comparePassword(password, user.password, (err, isMatch) => {
            if(err) throw err;
            if(isMatch) {
                const token = jwt.sign({data: user}, config.secret, {
                    expiresIn: 72400 //1 day
                });
                res.json({
                    success: true,
                    token: 'JWT ' +token,
                    user: {
                        id: user._id,
                        username: user.username,
                        email: user.email,
                        role: "user",
                        isBanned: user.isBanned
                    }
                });
                console.log(user);
                localStorage.setItem('validatsia', user._id);
            } else {
                return res.json({ success: false, message: "Wrong password" });
            }
        });
    });
});

router.get('/allUsers', (req, res, next) => {
    User.find({}, (err, users) => {
        if(err) {
            res.json({success: false, message: err});
        } else {
            if(!users) {
                res.json({success: false, message: "No users found"});
            } else {
                res.json({success: true, users: users});
            }
        }
    }).sort({'_id': -1});   
});


//Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    res.json({user: req.user});
});

router.put('/profile', function (req, res, next) {
    var path = '';
    upload(req, res, function (err) {
        console.log(req.file);
       if (err) {
         // An error occurred when uploading
         console.log(err);
         return res.status(422).send("an Error occured");
       }  
      // No error occured.
      User.findOne({_id: localStorage.getItem('validatsia')}, (err, user) => {
        if(err) {
            res.json({ success: false, message: err });
        } else {
            if(!user) {
                res.json({ success: false, message: 'No user found'}); 
            } else {
                console.log(req.file.path);
                const jpgPath = req.file.path;
                const arr = jpgPath.split("\\");
                arr.shift();
                arr.shift();
                const stringPath = arr.join("\\");
                user.avatar = stringPath;
                User.findOneAndUpdate({username: user.username}, {$set: {avatar: user.avatar}}, (err, user) => {
                    if (err) {throw err;}
                    else {
                        res.json({ success: true, message: 'Avatar updated'});
                        console.log("Updated");
                    }
                });

                console.log(user.avatar);
            }
        }

      });
 });     
});



router.get('/acessibleProfile/:username', (req, res) => {
    // Check if username was passed in the parameters
    if (!req.params.username) {
      res.json({ success: false, message: 'No username was provided' }); // Return error message
    } else {
      // Check the database for username
      User.findOne({ username: req.params.username }).select('username email isBanned avatar').exec((err, user) => {
        // Check if error was found
        if (err) {
          res.json({ success: false, message: 'Something went wrong.' }); // Return error message
        } else {
          // Check if user was found in the database
          if (!user) {
            res.json({ success: false, message: 'Username not found.' }); // Return error message
          } else {
            res.json({ success: true, user: user }); // Return the public user's profile data
            console.log(user.isBanned + " get http");
          }
        }
      });
    }
  });

  router.put('/acessibleProfileBan/:username', (req, res) => {
      console.log(req.params);
    // Check if username was passed in the parameters
    if (!req.params.username) {
      res.json({ success: false, message: 'No username was provided' }); // Return error message
    } else {
      // Check the database for username
      User.findOne({ username: req.params.username }).select('username email isBanned').exec((err, user) => {
        // Check if error was found
        if (err) {
          res.json({ success: false, message: 'Something went wrong.' }); // Return error message
        } else {
          // Check if user was found in the database
          if (!user) {
            res.json({ success: false, message: 'Username not found.' }); // Return error message
          } else {
            console.log(user.username);
           user.isBanned = true;
           user.save((err) => {
            // Check if error was found
            if (err) {
              res.json({ success: false, message: 'Something went wrong.' }); // Return error message
            } else {
              res.json({ success: true, message: 'User is banned!' }); // Return success message
            }
          });
          }
        }
      });
    }
  });

  router.put('/acessibleProfileUnban/:username', (req, res) => {
    console.log(req.params);
  // Check if username was passed in the parameters
  if (!req.params.username) {
    res.json({ success: false, message: 'No username was provided' }); // Return error message
  } else {
    // Check the database for username
    User.findOne({ username: req.params.username }).select('username email isBanned').exec((err, user) => {
      // Check if error was found
      if (err) {
        res.json({ success: false, message: 'Something went wrong.' }); // Return error message
      } else {
        // Check if user was found in the database
        if (!user) {
          res.json({ success: false, message: 'Username not found.' }); // Return error message
        } else {
          console.log(user.username);
         user.isBanned = false;
         user.save((err) => {
          // Check if error was found
          if (err) {
            res.json({ success: false, message: 'Something went wrong.' }); // Return error message
          } else {
            res.json({ success: true, message: 'User is banned!' }); // Return success message
          }
        });
        }
      }
    });
  }
});

module.exports = router;