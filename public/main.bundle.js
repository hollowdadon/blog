webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<div class=\"container\">\n  <flash-messages></flash-messages>\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_profile_profile_component__ = __webpack_require__("../../../../../src/app/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_accessible_profile_accessible_profile_component__ = __webpack_require__("../../../../../src/app/components/accessible-profile/accessible-profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_blog_blog_component__ = __webpack_require__("../../../../../src/app/components/blog/blog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_blog_service__ = __webpack_require__("../../../../../src/app/services/blog.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__guards_logged_in_guard__ = __webpack_require__("../../../../../src/app/guards/logged-in.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__guards_banned_guard__ = __webpack_require__("../../../../../src/app/guards/banned.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__guards_admin_guard__ = __webpack_require__("../../../../../src/app/guards/admin.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_update_post_update_post_component__ = __webpack_require__("../../../../../src/app/components/update-post/update-post.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_delete_post_delete_post_component__ = __webpack_require__("../../../../../src/app/components/delete-post/delete-post.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_23_ng2_file_upload__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















//Services



//Guard for links







var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__["a" /* HomeComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__["a" /* RegisterComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_18__guards_logged_in_guard__["a" /* LoggedInGuard */]] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_18__guards_logged_in_guard__["a" /* LoggedInGuard */]] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_10__components_profile_profile_component__["a" /* ProfileComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'blog', component: __WEBPACK_IMPORTED_MODULE_12__components_blog_blog_component__["a" /* BlogComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_19__guards_banned_guard__["a" /* BannedGuard */], __WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'edit-blog/:id', component: __WEBPACK_IMPORTED_MODULE_21__components_update_post_update_post_component__["a" /* UpdatePostComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_19__guards_banned_guard__["a" /* BannedGuard */]] },
    { path: 'delete-blog/:id', component: __WEBPACK_IMPORTED_MODULE_22__components_delete_post_delete_post_component__["a" /* DeletePostComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_19__guards_banned_guard__["a" /* BannedGuard */]] },
    { path: 'user/:username', component: __WEBPACK_IMPORTED_MODULE_11__components_accessible_profile_accessible_profile_component__["a" /* AccessibleProfileComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_20__guards_admin_guard__["a" /* AdminGuard */]] }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_blog_blog_component__["a" /* BlogComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_update_post_update_post_component__["a" /* UpdatePostComponent */],
                __WEBPACK_IMPORTED_MODULE_22__components_delete_post_delete_post_component__["a" /* DeletePostComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_update_post_update_post_component__["a" /* UpdatePostComponent */],
                __WEBPACK_IMPORTED_MODULE_22__components_delete_post_delete_post_component__["a" /* DeletePostComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_accessible_profile_accessible_profile_component__["a" /* AccessibleProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_23_ng2_file_upload__["FileSelectDirective"],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_13_angular2_flash_messages__["FlashMessagesModule"].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_14__services_validate_service__["a" /* ValidateService */], __WEBPACK_IMPORTED_MODULE_15__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_19__guards_banned_guard__["a" /* BannedGuard */], __WEBPACK_IMPORTED_MODULE_20__guards_admin_guard__["a" /* AdminGuard */], __WEBPACK_IMPORTED_MODULE_18__guards_logged_in_guard__["a" /* LoggedInGuard */], __WEBPACK_IMPORTED_MODULE_16__services_blog_service__["a" /* BlogService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/accessible-profile/accessible-profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".main-section{\r\n\tborder:1px solid #138496;\r\n\tbackground-color: #fff;\r\n}\r\n.profile-header{\r\n\tbackground-color: #17a2b8;\r\n\theight:150px;\r\n}\r\n.user-detail{\r\n\tmargin:-50px 0px 30px 0px;\r\n}\r\n.user-detail img{\r\n\theight:100px;\r\n\twidth:100px;\r\n}\r\n.user-detail h5{\r\n\tmargin:15px 0px 5px 0px;\r\n}\r\n.user-social-detail{\r\n\tpadding:15px 0px;\r\n\tbackground-color: #17a2b8;\r\n}\r\n.user-social-detail a i{\r\n\tcolor:#fff;\r\n\tfont-size:23px;\r\n\tpadding: 0px 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/accessible-profile/accessible-profile.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Public profile</h1>\n\n<!--<ul>\n  <li>Username: {{username}}</li>\n  <li>Email: {{email}}</li>\n</ul> -->\n\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"offset-lg-4 col-lg-4 col-sm-6 col-12 main-section text-center\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 col-sm-12 col-12 profile-header\"></div>\n        </div>\n        <div class=\"row user-detail\">\n            <div class=\"col-lg-12 col-sm-12 col-12\">\n                <img class=\"rounded-circle img-thumbnail\" src=\"{{ownerAvatar}}\">\n                <h5>{{ownerUsername}}</h5>\n                <p><i class=\"fa fa-envelope\" aria-hidden=\"true\"></i> {{ownerEmail}}</p>\n\n                <hr>\n                <span>Welcome in my app</span>\n            </div>\n        </div>\n        <div class=\"row user-social-detail\">\n            <div class=\"col-lg-12 col-sm-12 col-12\">\n                <a target=\"_blank\" href=\"https://bitbucket.org/hollowdadon/blog\"><i class=\"fa fa-bitbucket\" aria-hidden=\"true\"></i></a>\n                <a target=\"_blank\"  href=\"https://hollowdadon-alex-store.herokuapp.com/\"><i class=\"fa fa-cart-plus\" aria-hidden=\"true\"></i></a>\n                <a target=\"_blank\"  href=\"https://twitter.com/nodejs?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a>\n            </div>\n        </div>\n    </div>\n  </div>\n\n  <div class=\"container\">\n    <button type=\"button\" name=\"button\" class=\"dropbtn btn btn-danger\"  (click)=\"banUser()\"  *ngIf = \"!ownerIsBanned\">\n        Ban User\n      </button>\n    <button type=\"button\" name=\"button\" class=\"dropbtn btn btn-success\"  (click)=\"unbanUser()\"  *ngIf = \"ownerIsBanned\">\n        Unban User\n      </button>\n      <a routerLink=\"/blog\"><button [disabled]=\"processing\" type=\"button\" name=\"button\" class=\"btn btn-primary\">Go Back</button></a>\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/accessible-profile/accessible-profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccessibleProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AccessibleProfileComponent = (function () {
    function AccessibleProfileComponent(authService, activatedRoute, router, flashMessage) {
        this.authService = authService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    AccessibleProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            _this.username = profile.user.username;
            _this.role = profile.user.role;
            if (_this.role == "admin") {
                _this.currentUrl = _this.activatedRoute.snapshot.params;
                _this.authService.getAcessibleProfile(_this.currentUrl.username).subscribe(function (data) {
                    _this.ownerUsername = data.user.username;
                    _this.ownerEmail = data.user.email;
                    _this.ownerAvatar = data.user.avatar;
                    _this.ownerIsBanned = data.user.isBanned;
                    console.log(_this.ownerAvatar);
                });
            }
            else {
                _this.flashMessage.show('You are not admin on this website', { cssClass: 'alert-danger', timeout: 5000 });
                _this.router.navigate(['blog']);
            }
        });
    };
    AccessibleProfileComponent.prototype.banUser = function (username) {
        var _this = this;
        this.authService.banUser(this.ownerUsername).subscribe(function (data) {
            _this.flashMessage.show('User is banned', { cssClass: 'alert-danger', timeout: 5000 });
            window.location.reload();
        });
    };
    AccessibleProfileComponent.prototype.unbanUser = function (username) {
        var _this = this;
        this.authService.unbanUser(this.ownerUsername).subscribe(function (data) {
            _this.flashMessage.show('User successfuly unbanned', { cssClass: 'alert-success', timeout: 5000 });
            window.location.reload();
        });
    };
    AccessibleProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-accessible-profile',
            template: __webpack_require__("../../../../../src/app/components/accessible-profile/accessible-profile.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/accessible-profile/accessible-profile.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], AccessibleProfileComponent);
    return AccessibleProfileComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/blog/blog.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* Style The Dropdown Button */\r\n.dropbtn {\r\n    color: white;\r\n    border: none;\r\n    cursor: pointer;\r\n}\r\n\r\n/* The container <div> - needed to position the dropdown content */\r\n.dropdown {\r\n    position: relative;\r\n    display: inline-block;\r\n}\r\n\r\n/* Dropdown Content (Hidden by Default) */\r\n.dropdown-content {\r\n    display: none;\r\n    position: relative;\r\n    background-color: #f9f9f9;\r\n    min-width: 160px;\r\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\r\n    z-index: 1;\r\n}\r\n\r\n/* Links inside the dropdown */\r\n.dropdown-content a {\r\n    color: black;\r\n    padding: 12px 16px;\r\n    text-decoration: none;\r\n    display: block;\r\n}\r\n\r\n/* Change color of dropdown links on hover */\r\n.dropdown-content a:hover {background-color: #f1f1f1}\r\n\r\n/* Show the dropdown menu on hover */\r\n.dropdown:hover .dropdown-content {\r\n    display: block;\r\n}\r\n\r\n.card {\r\n    background:#FFF;\r\n    display: block;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    border:1px solid #AAA;\r\n    border-bottom:3px solid #BBB;\r\n    padding:0px;\r\n    margin-top:5em;\r\n    overflow:hidden;\r\n    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n    font-family: 'Roboto', sans-serif;\r\n    transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);\r\n}\r\n\r\n.card-body{\r\n    margin:1em;   \r\n   }\r\n   \r\n   .pull-left {\r\n     float: left;\r\n   }\r\n   \r\n   .pull-none {\r\n     float: none !important;\r\n   }\r\n   \r\n   .pull-right {\r\n     float: right;\r\n   }\r\n   \r\n   .card-header{\r\n       width:100%;\r\n       color:#2196F3;\r\n       border-bottom:3px solid #BBB;\r\n       box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n       font-family: 'Roboto', sans-serif;\r\n       padding:0px;\r\n       margin-top:0px;\r\n       overflow:hidden;\r\n       transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);\r\n       \r\n   }\r\n\r\n   .card-header-blue{\r\n    background-color:#2196F3;\r\n    color:#FFFFFF;\r\n    border-bottom:3px solid #BBB;\r\n    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n    font-family: 'Roboto', sans-serif;\r\n    padding:0px;\r\n    margin-top:0px;\r\n    overflow:hidden;\r\n    transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);\r\n}\r\n\r\n.card-heading {\r\n    display: block;\r\n    font-size: 24px;\r\n    line-height: 28px;\r\n    margin-bottom: 24px;\r\n    margin-left:1em;\r\n    border-bottom: 1px #2196F3;\r\n    color:#fff;\r\n   \r\n}\r\n\r\n.card-footer{\r\n    border-top:1px solid #000;   \r\n       \r\n   }", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/blog/blog.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Blog feed</h1>\n\n<div *ngIf = \"isBanned\">\n  <h3 class=\"text-danger text-center\">You are banned man</h3>\n</div>\n\n<button type=\"button\" name=\"button\" class=\"btn btn-warning\" \n*ngIf=\"!newBlog && !isBanned\" (click)=\"newBlogForm()\">New Post</button>\n\n<button [disabled]=\"loadingBlogs\" type=\"button\" name=\"button\" class=\"btn btn-dark\" *ngIf=\"!newBlog && !isBanned\" (click)=\"reloadBlogs()\">\n<i class=\"fa fa-refresh\" aria-hidden=\"true\"></i>  Reload Feed</button>\n\n<br>\n<br>\n\n  <form [formGroup]=\"form\" name=\"blogForm\" (submit)=\"onBlogSubmit()\" *ngIf=\"newBlog\">\n\n    <div class=\"form-group\">\n      <label for=\"title\">Title</label>\n      <div [ngClass]=\"{'has-success': form.controls.title.valid, 'has-error': form.controls.title.dirty && form.controls.title.errors}\">\n        <input type=\"text\" name=\"title\" class=\"form-control\" placeholder=\"*Blog Title\" autocomplete=\"off\" formControlName=\"title\" />\n        <ul class=\"help-block\">\n          <li *ngIf=\"form.controls.title.dirty && form.controls.title.errors?.required\">This field is required.</li>\n          <li *ngIf=\"(form.controls.title.dirty && form.controls.title.errors?.minlength) || (form.controls.title.dirty && form.controls.title.errors?.maxlength)\">Max length: 50, Min length: 5</li>\n          <li *ngIf=\"form.controls.title.dirty && form.controls.title.errors?.alphaNumericValidation\">Must be a letter or number</li>\n        </ul>\n      </div>\n    </div>\n  \n\n    <div class=\"form-group\">\n      <label for=\"body\">Body</label>\n      <div [ngClass]=\"{'has-success': form.controls.body.valid, 'has-error': form.controls.body.dirty && form.controls.body.errors}\">\n        <textarea name=\"body\" rows=\"8\" cols=\"80\" placeholder=\"*Body\" class=\"form-control\" formControlName=\"body\"></textarea>\n        <ul class=\"help-block\">\n          <li *ngIf=\"form.controls.body.dirty && form.controls.body.errors?.required\">This field is required.</li>\n          <li *ngIf=\"(form.controls.body.dirty && form.controls.body.errors?.minlength) || (form.controls.body.dirty && form.controls.body.errors?.maxlength)\">Max length: 500, Min length: 5</li>\n        </ul>\n      </div>\n    </div>\n  \n    <button [disabled]=\"processing\" type=\"button\" name=\"button\" (click)=\"goBack()\" class=\"btn btn-warning\">Go Back</button>\n    <button [disabled]=\"processing || !form.valid\" type=\"submit\" name=\"button\" class=\"btn btn-success\">Submit</button>\n    <button class=\"btn btn-primary\" type=\"button\" name=\"button\" (click)=\"refreshForm()\">Refresh all Form</button>\n  \n  </form>\n\n\n\n\n<div class=\"row\" *ngIf=\"!newBlog && !isBanned\">\n  <div class=\"col-md-4\" *ngFor=\"let blog of blogPosts\">\n  <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"card-header-blue\">\n                 <h1 class=\"card-heading\">{{blog.title}}</h1>\n               </div>\n               <div class=\"card-body\">\n               <p class=\"card-p\">\n                {{blog.body}}\n                      \n                  </p>\n             </div>\n             <div class=\"card-footer\">\n              Posted by: <a [routerLink]=\"['/user/', blog.owner]\">{{blog.owner}}</a>\n              <br>\n              Date: {{blog.dateOfCreating | date:'MMM dd, yyyy'}}\n              <br>\n             <div *ngIf  =\"username === blog.owner\">\n               Likes: {{blog.likes}}\n               <br>\n               Dislikes: {{blog.dislikes}}\n             </div>\n             <a [routerLink]=\"['/edit-blog', blog._id]\" *ngIf=\"username === blog.owner\"><button type=\"button\" name=\"button\" class=\"btn btn-sm btn-info\">Edit</button></a>\n             <a [routerLink]=\"['/delete-blog', blog._id]\" *ngIf=\"username === blog.owner\"><button type=\"button\" name=\"button\" class=\"btn btn-sm btn-danger\">Delete</button></a>\n             <div class=\"dropdown\">\n               <button type=\"button\" name=\"button\" class=\"dropbtn btn btn-success\" *ngIf=\"username !== blog.owner\"  (click)=\"like(blog._id)\">\n                   <i class=\"fa fa-thumbs-up\" aria-hidden=\"true\"></i> Like: {{blog.likes}}\n               </button>\n               <div class=\"dropdown-content\" *ngIf=\"role == 'admin'\">\n                 <a [routerLink]=\"['/user/', likedBy]\" *ngFor=\"let likedBy of blog.likedBy\">{{likedBy}}</a>\n               </div>\n               <div class=\"dropdown-content\" *ngIf=\"role !== 'admin'\">\n                <p *ngFor=\"let likedBy of blog.likedBy\">{{likedBy}}</p>\n              </div>\n             </div>\n             <div class=\"dropdown\">\n             <button type=\"button\" name=\"button\" class=\"dropbtn btn btn-danger\" *ngIf=\"username !== blog.owner\"  (click)=\"dislike(blog._id)\">\n                 <i class=\"fa fa-thumbs-down\" aria-hidden=\"true\"></i> Dislike: {{blog.dislikes}}\n             </button>\n             <div class=\"dropdown-content\" *ngIf=\"role == 'admin'\">\n              <a [routerLink]=\"['/user/', dislikedBy]\" *ngFor=\"let dislikedBy of blog.dislikedBy\">{{dislikedBy}}</a>\n             </div>\n             <div class=\"dropdown-content\" *ngIf=\"role !== 'admin'\">\n              <p *ngFor=\"let dislikedBy of blog.dislikedBy\">{{dislikedBy}}</p>\n             </div>\n           </div>\n           </div>\n          </div>\n      </div>\n   </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/blog/blog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_blog_service__ = __webpack_require__("../../../../../src/app/services/blog.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BlogComponent = (function () {
    function BlogComponent(formBuilder, authService, blogService, flashMessage) {
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.blogService = blogService;
        this.flashMessage = flashMessage;
        this.newBlog = false;
        this.loadingBlogs = false;
        this.processing = false;
        this.comment = [];
        this.createNewBlogForm();
        this.newComment();
    }
    BlogComponent.prototype.createNewBlogForm = function () {
        this.form = this.formBuilder.group({
            'title': ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].maxLength(50), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].minLength(5)]],
            'body': ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].maxLength(500), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].minLength(5)]]
        });
    };
    BlogComponent.prototype.enableNewBlogForm = function () {
        this.form.get('title').enable();
        this.form.get('body').enable();
    };
    BlogComponent.prototype.disableNewBlogForm = function () {
        this.form.get('title').disable();
        this.form.get('body').disable();
    };
    BlogComponent.prototype.newBlogForm = function () {
        this.newBlog = true;
    };
    BlogComponent.prototype.reloadBlogs = function () {
        var _this = this;
        this.loadingBlogs = true;
        this.displayAllBlogs();
        setTimeout(function () {
            _this.loadingBlogs = false;
        }, 3000);
    };
    BlogComponent.prototype.onBlogSubmit = function () {
        var _this = this;
        this.processing = true;
        this.disableNewBlogForm();
        var blog = {
            title: this.form.get('title').value,
            body: this.form.get('body').value,
            owner: this.username
        };
        this.blogService.newBlog(blog).subscribe(function (data) {
            if (!data.success) {
                _this.flashMessage.show(data.message, { cssClass: 'alert-danger', timeout: 5000 });
                _this.processing = false;
                _this.enableNewBlogForm();
            }
            else {
                _this.flashMessage.show(data.message, { cssClass: 'alert-success', timeout: 5000 });
                _this.displayAllBlogs();
                setTimeout(function () {
                    _this.newBlog = false;
                    _this.processing = false;
                    _this.form.reset();
                    _this.enableNewBlogForm();
                }, 2000);
            }
        });
    };
    BlogComponent.prototype.refreshForm = function () {
        this.form.reset();
    };
    BlogComponent.prototype.goBack = function () {
        window.location.reload();
    };
    BlogComponent.prototype.displayAllBlogs = function () {
        var _this = this;
        this.blogService.getAllBlogs().subscribe(function (data) {
            _this.blogPosts = data.blogs;
        });
    };
    BlogComponent.prototype.like = function (id) {
        var _this = this;
        this.blogService.likeBlog(id).subscribe(function (data) {
            _this.displayAllBlogs();
        });
    };
    BlogComponent.prototype.dislike = function (id) {
        var _this = this;
        this.blogService.dislikeBlog(id).subscribe(function (data) {
            _this.displayAllBlogs();
        });
    };
    BlogComponent.prototype.addComment = function (id) {
        this.comment = [];
        this.comment.push(id);
    };
    BlogComponent.prototype.newComment = function () {
        this.commentForm = this.formBuilder.group({
            comment: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].maxLength(100), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* Validators */].minLength(3)]]
        });
    };
    BlogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            _this.username = profile.user.username;
            _this.isBanned = profile.user.isBanned;
            _this.role = profile.user.role;
            _this.avatar = profile.user.avatar;
            _this.displayAllBlogs();
        });
    };
    BlogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-blog',
            template: __webpack_require__("../../../../../src/app/components/blog/blog.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/blog/blog.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__services_blog_service__["a" /* BlogService */],
            __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"]])
    ], BlogComponent);
    return BlogComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/delete-post/delete-post.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card {\r\n    background:#FFF;\r\n    display: block;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    border:1px solid #AAA;\r\n    border-bottom:3px solid #BBB;\r\n    padding:0px;\r\n    margin-top:5em;\r\n    overflow:hidden;\r\n    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n    font-family: 'Roboto', sans-serif;\r\n    transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);\r\n}\r\n\r\n.card-body{\r\n    margin:1em;   \r\n   }\r\n   \r\n   .pull-left {\r\n     float: left;\r\n   }\r\n   \r\n   .pull-none {\r\n     float: none !important;\r\n   }\r\n   \r\n   .pull-right {\r\n     float: right;\r\n   }\r\n   \r\n   .card-header{\r\n       width:100%;\r\n       color:#2196F3;\r\n       border-bottom:3px solid #BBB;\r\n       box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n       font-family: 'Roboto', sans-serif;\r\n       padding:0px;\r\n       margin-top:0px;\r\n       overflow:hidden;\r\n       transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);\r\n       \r\n   }\r\n\r\n   .card-header-blue{\r\n    background-color:#2196F3;\r\n    color:#FFFFFF;\r\n    border-bottom:3px solid #BBB;\r\n    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\r\n    font-family: 'Roboto', sans-serif;\r\n    padding:0px;\r\n    margin-top:0px;\r\n    overflow:hidden;\r\n    transition: box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);\r\n}\r\n\r\n.card-heading {\r\n    display: block;\r\n    font-size: 24px;\r\n    line-height: 28px;\r\n    margin-bottom: 24px;\r\n    margin-left:1em;\r\n    border-bottom: 1px #2196F3;\r\n    color:#fff;\r\n   \r\n}\r\n\r\n.card-footer{\r\n    border-top:1px solid #000;   \r\n       \r\n   }", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/delete-post/delete-post.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Delete Post</h1>\n\n<div class=\"row show-hide-message\" *ngIf=\"message\">\n  <div [ngClass]=\"messageClass\">\n    {{ message }}\n  </div>\n</div>\n\n\n <div class=\"col-md-6\" *ngIf=\"foundBlog\">\n  <div class=\"modal-content\">\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Confirm deleting</h4>\n    </div>\n\n    <div class=\"modal-body\">\n      <p>Are you sure?</p>\n    </div>\n\n    <div class=\"modal-footer\">\n      <button [disabled]=\"processing\" type=\"button\" name=\"button\" class=\"btn btn-success\" (click)=\"deleteBlog()\">Yes</button>\n      <a routerLink=\"/blog\"><button [disabled]=\"processing\" type=\"button\" name=\"button\" class=\"btn btn-danger\">No</button></a>\n    </div>\n  </div>\n\n  <br />\n\n</div>\n<div class=\"col-md-12\">\n    <div class=\"card\">\n      <div class=\"card-content\">\n        <div class=\"card-header-blue\">\n            <h1 class=\"card-heading\">{{blog.title}}</h1>\n        </div>\n        <div class=\"card-body\">\n            <p class=\"card-p\">\n             {{blog.body}}\n                   \n               </p>\n          </div>\n          <div class=\"card-footer\">\n              Posted by: {{blog.owner}}\n              <br>\n              Date: {{blog.dateOfCreating | date:'MMM dd, yyyy'}}\n              <br>\n          </div>\n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "../../../../../src/app/components/delete-post/delete-post.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeletePostComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_blog_service__ = __webpack_require__("../../../../../src/app/services/blog.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DeletePostComponent = (function () {
    function DeletePostComponent(blogService, activatedRoute, router) {
        this.blogService = blogService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.foundBlog = false;
        this.processing = false;
        this.blog = {};
    }
    DeletePostComponent.prototype.deleteBlog = function () {
        var _this = this;
        this.processing = true;
        this.blogService.deleteBlog(this.currentUrl.id).subscribe(function (data) {
            if (!data.success) {
                _this.messageClass = 'alert alert-danger';
                _this.message = data.message;
            }
            else {
                _this.messageClass = 'alert alert-success';
                _this.message = data.message;
                setTimeout(function () {
                    _this.router.navigate(['/blog']);
                }, 1000);
            }
        });
    };
    DeletePostComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentUrl = this.activatedRoute.snapshot.params;
        this.blogService.getSingleBlog(this.currentUrl.id).subscribe(function (data) {
            if (!data.success) {
                _this.messageClass = 'alert alert danger';
                _this.message = data.message;
            }
            else {
                _this.blog = {
                    title: data.blog.title,
                    body: data.blog.body,
                    owner: data.blog.owner,
                    dateOfCreating: data.blog.dateOfCreating
                };
                _this.foundBlog = true;
            }
        });
    };
    DeletePostComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-delete-post',
            template: __webpack_require__("../../../../../src/app/components/delete-post/delete-post.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/delete-post/delete-post.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_blog_service__["a" /* BlogService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], DeletePostComponent);
    return DeletePostComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron text-center\">\n  <h1>Blog app with like/dislike, creating new post, edit and delete related posts</h1>\n  <p class=\"lead\">Welcom to my custom Mean APP</p>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-4\">\n    <h3>Express Backend</h3>\n    <p>Node.js Server usig Mongoose and Passport</p>\n    <img style=\"width: 400px; height: 200px;\" class=\"img-fluid\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhIVFRUXFRUXFRcVFRUVFRcVFhUWFxUVFhUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGisfHR0tLS8rLS0tLSstKy0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS4tLS0tLS0tLS4tLS0tLf/AABEIAJ4BPgMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBAECBQAGB//EAEMQAAEDAgMFBAcGBAQGAwAAAAEAAgMEERIhMQVBUWFxBhORsQciMoGh0fAjQlJyweEUJLLxYmNzgzM0Q1OCkhWztP/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EADMRAAICAQMBBAYKAwAAAAAAAAABAhEDBCExEiJBUXETM4GxwfAFFDI0QmGCkcLRIyVy/9oADAMBAAIRAxEAPwD5I7PK6s2Adeqq/L65j5rnTt4j9fBCGwoiCpIbNN0D+Kv7ILvgFdseIHHnyGgzQAzT5tBQaxoR6PQjg4qtVzCkfeZr4lTCn8CEWZosYqGqzWo3dq4aiwoiJu9Xw3U2VgkAWnltkfH5poJRy6KW2R08v2TTJaHVSQZKwKlUSVYbgKypG2wtwQZq1jdTc8BmUAMrnPAFybdVkTbUccmi3XMqjaSWTM36u/QIGOz7TYPZu48sh4pKSukebNy5NGfjqnYdlNHtEu+ATscYaLAADkmIx4tmPdm44euZT8OzWN1GI8/kmHTAc+iH3jjpkgA2Q5IbpuAuhuDRm45+JQnVY+6PH5IoLD+sd9hyQnPY3fc8vqyWdI53E8v2RGUpOuXxKYrOfVHcLfEoNZcxXO5/6funo6UDd45/BC2kPsnciPNIaIpvYb0HkiFCoj9m3ojAKGWVIQXhGchuCkaFntR423bY+79EN7Hai3vUGfUPBB49FSBq+AhZlYm3TJLSgk4QSeO+wVJ33FjnbRBjdY3BN96FGhXYxh3aW3fqFBXOlJzAVTi4BBVD8/15/orlgvmoqW+r4+RVmZgKkQyz2ZZJZziL9D5Jxh3Jd7LG+5AIjZklwb8fNMObivxvYe5ZEWTiAclp0km471LGUe22SGU9OQBmLpJJgiNFTvAuld9aIbWk7uf0Smq7x0xhvnmuRrDDa+4ZZrmMyBAB434Jc8C4BIndqxjz5KUUOyDIGakAc/0CXm2qPui/M5DwSVRnIcR+94D+y0IqMAkBl7aF2e/wVpEMTLpZONv/AFaixbPH3nX5N+ZWiIfxOvlpuVTNbJo9/wBfNMQSnp2NHqtA8/FS+do336fPRBDXO1JV+6AudbeKAOE5OgUOafvHLnoh99vyaD7yhXucgSc887fsmIOHDcMXXIIc8pt7XubkrNpydT7h80ZkLW8viUAIsgcd3imY6Mb8/gE2GoU5zAxW46XSsdHNAGQ8Aoc11tbcbangodLbLMnpmoAfra3mgCWTG5BzPBqHXt+zf4/FFDMRvi04ft+6irHqP/KfJIYns8/Zj3+aaCS2a71DyJ/RHdIfr9T70qtjugpQ3BR3hGZNx5IjQpaGUsqOcD6pt70V6SnFzkmAOWnzyUhwa3AczxUF7gM8wq4HG2QTQNnEfspUSNsATqfJVDkUOzUnbdpSArDucPeDf4XTdNVY3EWyGiWqaB17tFwgk4VxH3gf/E/rZDnrC7j5DwHzXN2e87gOpCNHst28gdM07AXpoybnhmUw1y0KenawWHvO9EEQve2aljszy4nmrlpCebEBnYIE8NswlQWAIXK/dHgqSMI1SGQEWE7uB8/ooKJHlbmmuQfAclQFygJslGZtVlnA8R5fQWrDIXNB4gacd/xSW1GXbfgfPL5I2yH3jI4E/HP5qkJjGG2ZIHXMqC/g33nJS2Ma/uVZttwvzKYjsJOv7KzG7lx5lQ05oAFHStHPr8kXIc+X10VyNeqqGnjbpn9ZoA57vdl81AaeHj71YWGYHv8A3V2OuLoAhreaBLGQ67Rc77+CtIx+IEPs3e0gfA6ouI7h4pDBMc7FbDYC9z5KYr3Opuchr0tZbex+zc1RY2ws/G7If+I1d5c17GCgo6CPvXua0jWR+bjyYB5NF1xajX48T6V2peCMp5VHblnkqPsjVPbiDGxjcHus4+4A299lk7S2e+LEyVha7Cdd4tqDoR0W1tP0nHvAKeEGMHMyEhzx/hA9nqb9F6PZ226PaMfdPaLkZxSWvfix2/q3Pouf61qsfbyw7P5coj0k1vJbHyClP2bvzBEEuQA4EL3u2OwJjDjS3e05mNxGPo1xycORseq+fzsLHFrmkEHMOBDgeYOi78Gpx5lcHZtGSkrQS+Qvu/VOQeyOizg+5T8b8lqzTkbpqCSW+ANNr3vJGw2AuTZ7gSAATfkgVNLJG0PkZ9mXFocHMeMQFyLtJ3fqmNm1DWPLnGw7uZuhObontbpzITtDtKFsUcUmYaXyEYSftGPY+JvRw7xp4YlhOeSMtla8vMhtmJtGkMb3RyDA5trtJaSMTQ4b+BBSnfBosM0ztqo72XHfFdkIJItd7YWNfl+YOWa5a42+lXz3lVa3Oe8uNyoJULlYGzTQhgsPeeKOELvQlxXjFax15cbKQHwrAoMsuHMgnoLrmzg/df8A+pQAcKQhMe06H5jqFayACXUXVbHiuueI8EATdLSwkm6KZDwVTOeHx/ZIaF3RW1VgCVeWa4tZDgnANjceSQw4UEqxaquHFaPghcgpmXaRxCU2I/1y3iPiP7lPLLjOCYfm+Dv7pRGzYOW6/XRSXcf2UzDNCbGTnb3lDsRckblwUmw1Kr3/AAHh80wGN/uQGlx0FuZ+SKL2CnBxzQAERjeS4orW7tByVJqljPaIHLf4LPn2sfuNtzPyTEeg2ZsqWZ2GJhdxcfZHV27zXttldlYYB3k5D3AXJdlE22+x16nwCa2jVNoKPEyPEGBrQ0EDE5xAxOPMm5K+Xbb2zU1Z+2ksy+UbMmD3bzzN146nn1t9D6IcX3v5+bMOqWTjZHse0XpFjjvHSASO0xnKMflGr/gOq+d11VPUP7yZ7nu4u0HJrRkByARoqYCwAzOmRJPQaldO4sIBjff/ABNLcuIBXfp9LiwbRW/j3mkYRjwLx0Y35pmJo3EZcNyWaXvOEXcTkGxi5PS37rSoez1bcEUkoadbtIPX1reS6JZIx+00im0uT0+wu2U0VmzXlZxv9oB+Y+17/FeoqqKi2lHfJxA9pvqys67/AHG4Xzup2dNGLyRSMHFzHAeNrIUUr2kOje5jho5psR+3Jefl0OPI/SYX0y8VwZPGnvHZje3+xNRTXez7aL8TR67R/jZ+ouOiwYnL6V2K7WSVEn8PM0Yw0uD25XwkAhzdxz1HgvMdv6JkVYcADQ9jZCBkMRLg6w54b9SUtPqMvpPQ5l2krtd5eOcurplyYOJDcVxK5dxuBkCXcEy9Dc1NALlVV3BVsqJNJr80SGBmI5etrnz/ALJRxsUaH28XG48LKUimOzQlwtex1BCrDMb4X67juKI1y6RgIzRRNkyxA56HcRr+45KjZyDhfYcD90/I8lLXkZHx4qz2gixzCYi91UuHFavZLZsd56ipu6kpYxI9oNnve5wZDAHbg52/gCnx2+rjnBM2CL7kUEUTYmD8IaWnF/5XQB5h2qqDdeu2qGVlI+tbG2Kpp3xtqRE3AyWOU4Ypw0ZNkDxZ1tb34BW2nG3aNMauNoFXTsArGNAHfQtybVsaMsQ0eB14XlopM8Y8IL2jRe12NE2hpxXyta6eYObQxOAIA0fWPadWi9mA6k30sQ12Rhgds+vlqgXxxy00jwDZ8h+1wxB4zbjkcwE8CSlQ7PnrawtFsjbidEzFNiaCvQydvNotyhmbBGPZhgiiZCwfhDS03H5iUXaOCto31rYmRVVO9gqmxNwRyxTEtjqAwZNkDxZwGRvfgFURSPN3WZtNoxAjhn1C+p1L6WDZmzqmWJk0xjqGQxP/AOEXd+4vmmAsZGtGABu8vz4jIp6gVpkq9o4X01DHjMUUbIQ90rgyGnHdNBDXPGbjcgNPG6a2YjzIqAWB+WmfLJDMpcNbcDuXpaL0g172nupW07AbMhgiiZExuVmhpabj810fbTYKukfX93HHUU72Mq2xANZIyUkRThgyY/GMLgMje/JMR5DAL53Jy10vyRTHxdYdbL1Nbtx+z6GikomMZJVMlkkqXMZJJdkhZ3MZcCGBoAvbe4aJPYvbuoqJWQbSc2qpJHBkvfMYDG15DTKyUAOY5t73vuPUMR56XabG5Nu4+A8Vnz7Re7fhHBvzXruxuxIP42qxNbVspoauaBmrKh0BAiuB7TSDisNbb9Fd3pC2kPWFX3bdzGRQNiA3NbHgII6396Bnio6dxz+JR/4QWO82K9P2o2hT1EEFXE2JlS4yx1UUdmNLmEGOo7oexjBN9xI6rylRITq46DIaacf7poR9g9ITgKF5P4otPzhfJX1DrZDDzOpX1j0i/wDIP/PF7vXGa+RRxa33+fG/j4ry/oj1D837kY4PsnoOwLv5+DUn7XM/6b8vrit30sgmSmt+GXLjmzK29YvYCO1dD/uf/U9b/pSv3lMQMw2QjqHR2Syv/YQ/5f8AIbX+VeX9noNl7Np9mUxkcM2tBlktd7nfhHK5sB+5WHL6TG6tpjbi+QMJHEANK9PDNBtCmIzLXgY2g2exwsbHgQRroeYXlq30Yscbx1LhxD2Bx8QR5LzsD07lL63fXf5/Awj02+vk1+z3beCqf3RY6N5yAdZzXcgR+oWP242IyEtmiGFryQ5o0D7XBA3AgHLlzSJ7A1kL2vhfE8tIIzLX3BBFsQw6gbwu7QbTrHAQ1Iw54gMDRci4uHDUZ7iu3T4sazqWmmunvV/A0jFdVwewTsFH/O4v8p4PiyyF6TP+cb/oM/rlTPYH/m/9p/m1LekwXrGj/IZ/XKtJff8A9Ja9b7DyQKoXokrbJcvXoo6SDKQqukzuqOKhVRJfGoJCoVxRQWMyNtkiU53c7+I+YK6obmuAsWniLHzH6pPkf4UOtKuCgtKI0oEEsuA4KGqzngC5NggR7LslWMZs7aBMMc+F9I90cmPCWY3txeoQfVcQdVk0/aanxWbsqjDSCQb1GdrZ27zTP4LP2Dt91LKZSwPhex0UsL8hPE/2mH8JyBB1BHuWmNn7Lc7vGV1RA0/9GWkMsjR+ESxvDSOBPvQA67bb30tWKfZ9PFGWRNqJIzJdrXSDu8nyG5Lm5WB3qnYOIwvdtF73Mgpvaw2xTyOb6tK0HI4gfW4DPLIgO1drwGFlFStkjp+8D5pZQ100z/Zxua02DWi+FgPXNA7T7ZZL3cFO1zKWAWia62J7j/xJ5LZGRxv0HC5SY0Pdum/xGDaURLoJg2Mty/lZY2gGmIAFmW9ZpsLgn3j2NGTsjaJGjZqEu6d44eZCR7M7dZTufHUNMlJO3BURjW2rJWcJGHMHrytrdjNqMpqHaL2j+Ig/iaaMtkBZ30EgmY5rgL4HFpvlo4DVOtrC96PFlq9B2RyptqyE+oKSOM/6ktQ3uvf6rkV1Bspxxtr6iFuvdS0ZlkHLvY3hh62SO3O0EBhbQ0UcjKdr+8kfKR31RMBhDpMOTWAaMHXVKKGx/tSf5DZX+jV//pTPZOsYzZm0C6COfDJRufHLjwljnSMafUcDk4jxWPt3ajJaWhhZfFTxztkuMryTY24TvyQeze2zSSFxjEsUjHRTwuJDZYXe00nccgQ7cR1TezEt0Hj7UQi4/wDiKJt9c6nPwlTTttvkoqxtNs2mghc2FlTLGZLtDpQYhZ8huS5p0BXOoNkyHEyvqIW74paN0srf8PexvDDwv4oG3dsQ9wyioo3sp2v72R8pb31RNbCHvDfVaxo9lg66qiQWzNq1lDGIHsZJBKxk4gqo2TQubILsla2923AOhB4pmDa2yZ3iOoozSFxA7+kmeWMJyu6nlu0MG/Cb2uq//LUdTTQ020O9ifA3u4KqFoktFmWxTxEgua3PCWm+fUmtJTbHgcJXVM9dhIIhjpjTte4ZgSyyOJDDYXsLoAVn2dXUdVUd0S11C8Y5o7DDidhjfYm5a+4FrHJ1ijO7ZQzn+c2ZS1DtXSRh9JM82zc50Jwk7/Z9yps/ti4V09TOxszasPZUwAlrXRvt6jXfdc3C3CczlrndEdszY7yXxbRqIWnPu5KIyTNH4e8jfgPXJAAe0ex6Y0sddRukZC6buJIZ7OfDNg7wBr2j12FoyNgcs9cvMSkAWtfLXTXMZL03aDa9L/Ds2fSNkEDZjNJJNh72aYtwBxDThjYG3AAuc7nO9/N4tW2tkeZG/M/WqaA+vekUXoH/AJov6wvlDYua+rekYH+Afb8UX9YXyZsR42+K8n6J9Q/N+5Gem+z7T0vYRgFdD/ub/wDLevR+kCk76opIg7CX42gnQXczMrzPYOIivhOL/uZf7b16H0lyFstM4GzmiRzTwIdGQfEKc1/XoVz0v+Q5euXl/Zl7a7Oy0LWStmvidhuzEwg2LhnfQ2Pgloe1Va3/AK7j1ax3xc0le32dtykr4u6kwhzhZ8TzY3G9h355gjMcktL6PqYn1ZJmjgCwjxLbrOOsgl0aqPaXikQsq4yLfyMSj7fVDT9qxkjd9hgdbfY6X9y9X2xga+ke4jNgD2neDcX8QSEpR9iaSEiR5e/Dn9o5oYLbyAAD78li9u+1kUjDTQODwSO8e32bAghrT97MC5GWVt+WNY8ueD08ap7vhfPvJqM5roXmK+jp5NUD/lSebVf0it/m2n/IZ/XKgejR16o/6b/NiP6R3fzTf9Fn9ci7Zff/ANJaX+b2HjagpQpmV10o45r0kdDKPVWqzlCok5Q4KQougDSqm6eCFNniA3Wt7kw6xAPO6Xg16qXyVHgYiNxdGak434LtdkN3ME6Ij5ja/sDifaPQIEHmnDRxPAeaC2J8lnkho1aLX955oUYLtG+rvLjm7qVotdkgQGGnzxOJcd193u3JkqGlTdAHBUKIhyIYxKukystvZe0mR0NRSlrsU0sDw4WwgRY8Qdne5xC1huWB7T+idATyOuz4ChxfidIcllRvzvz81oVXs5fWSzIhnZSijV3IblaI5Ksgt9fXNVLdEx5IjfZ3XI/omHNSZCcgfiaDvGRSixzQKWO4I81mOY77xt1y8AFtELNroQHYjv4C+ashAi4ZkZkW1yHM2HPzUuBIBJt1yB33tv36ckNrzezBY+LkR7cgXnPMfiJA+F80DJOG4BBNsidByPE7t6n1jcOyB36AEadcwqSyWsQBoLE5nLLoD+yv3D32dbPQk8tD4eSAPr+ytp0u0qfu3kF2Ed5HctcHNt6zd5F8wfHgvIdouw9RDd9PeeP8NvtQOgyf7s+S83S0WAh4e4OGYLSWkHkRmvZ7K7cSxgNnb3wH3m2bJ7x7Lvh715D0ubTyctO7i/wv4fP7mPTOG8OPAz/R/suo/i2yPiexkYeXF7XNzLS0NFxmbn4Jz0sTDvadt8wyQnoXNA/pK1ar0iwBvqQyuduDsDRfm7EfJfP9sS1NW59W+NxbkC5rXGNjRo0HgPM56owwzZNQs2WPSkqXz7Rx6nPrltQhiRmbRmbk2WRo/wAMjwPgUs1psj7P2fLO/u4Y3PdwbuHFxOTRzK9OXTVy4OpvbcpPVvf7b3v/ADOc7zK1Ng7AqKs/ZM9XfI7Jg9/3jyF17Ps/6PI2WfVkSO17sXEY/MdX/AdU1t3tvT047qnaJXgWAblEzlca9G+IXmz1zm/R6aPU/HuRzvO3tBWPbB7O09C0yufd9rOkecLQDa4aNwyGtyvB9s9qtqJzIz2QAxp0JDSTe265cfdZK7W2zNOQ6Z5dwGjW/lbu81jvmJK002klCby5JdUn+yHjxtPqk7ZRzkIlFceKFzXoGpVwQ3IjlQhMRAUlVUkoEPNf6pPHz0XQ6hD0HUosQWZoMSMB1/fxQ4aUXuc+uaMrMVEBAuUAqwQBYBWUKUCJS1bJYJlZtScTrD6srh4+BEvDxLUbMr8U2qxtsFdZc7motVcvr6slTHY34+e9M1TwHC/19XU4bjL3JS23LRWA6hXmbcIAOFc6Z54N+JVpqiGnexxV6R1nW3Hz3IW5QCoTopqzTIQqmLE21r8Ou5FhfiaD49d6tZamRhxOLrsta4yAFsxx8s0xT0BIs7IZEbzf+y0wwDQWVJZA0XdkEADhpmtGl7Z3Oef6aI4Wc/anBvibK8O0gcnC3xCAHrKkzrAlX5qrkhmZ/EEEO3ggi4BFwbjI5EcjkvonZz0hROAiqmiI6B7R9kd2bfufEdF4CSmJKp/BLn1Glx541MnJFTW59Wr+xNHO9srbsBzcIiMDxyyIb1b+6JXbYotnM7pjRi3RR5vJ4vcdOrjfqvmNJUTxtwxzSsafute5o8AckIRb9+pOpJ4krjX0dOTrLkcorhGXom9pO0am3+01TV3a493F/wBthNj+d2r/AC5LDbf2Tkdx4jgnA1LVcjLWJz3WzK9LHjjjXTBUjVRSVIq69rJdxRoJ8QsdUs5UaXsWJVbqLqCUCIKglSFVyYiFy5cgB1+oHJFjCCHAm6MHDiFmaB49FcBUjRAVRBZoVwh4go74bs+gugQZSELvDuHifkpz4+ARY6JnksCk6O1yVNbkABqfejU8dgAqcqjSJUe1bDBcuuoKzLEKnOToPr4JhqTL/XJ4HyFkR04brmVE1Za4CTN3+KFdC71zuQRGJpUgJsuIVrKY23KAGKLLI78/f9eSbWfI/PLd5p9jrgFaRZnJEOWHXylzzwGQ/Vbb+PD6Kw62LC88zccwVRIBcuXJga2yZbtLTu8im0ns2PC0k6u06DenEgOKhUmqGt1Pu3+Czpq5x9nIfFAzQkka3U2+uCVl2g0eyL/ALPsTmc1IalYUXlqXu1NhwGSGGq4auKVlUEjZccxoguOeaK02VXtueaEwaKEqFxFlCokglQuK5AHLly5ABXlVa48SpcrRBSUXiqC3n1UmpfxVsCrgRYUcJzyVxVuUCNT3SQywrXcPrwVxXnghGJULEBQU1ILsRGfT90YVw+r/ACSRYqkIA0f45v0f2VXVl9LeKzXFUxJ0IYjOvNRGwb0EOKMxJoaYUFEYUEFEBUlFyjjIIEequ9yQMglMUEmrT1H6pR+tlLZLHFw8uCpCatGqkaxnFuJvL2m9PkmKh3qlCp6i7blXZKjtZm9yw6SW6tN/grM7tuebz0s35q1aQfWAt+qVCLFSHmVgF3HNxyAGgCDLWPdyHL5oFkRrUWOimFThVyoSGVKkBcVKAIXLiuQBy5hzUXUIAtUi5ugJnUIMgTQmu8ouUKUyTly5cgD/2Q==\">\n  </div>\n  <div class=\"col-md-4\">\n    <h3>Angular-CLI</h3>\n    <p>Angular and Angular-CLI for front-enders</p>\n    <img class=\"img-fluid\" src=\"https://octoperf.com/img/blog/angular2.png\" style=\"width: 400px; height: 200px;\">\n  </div>\n  <div class=\"col-md-4\">\n    <h3>JWT Tokens</h3>\n    <p>For authentification using JWT wevbtokens</p>\n    <img class=\"img-fluid\" style=\"width: 400px; height: 200px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT0AAACfCAMAAAC85v7+AAABVlBMVEUAAAD///9zc3MA8ub7AVvWOv8AufHQ0NATExPw8PCFhYVubm729vaYmJhGRkYkJCRiYmKSkpLY2NgaGhrp6emHh4cA+OwICAjcPP8AwPozMzMAnpaXATfy8vK7M98Aqt7xAVe8vLykADwkAA3MN/Ourq4Ag6vj4+MAExmZKbcAu7J8fHxXV1cAmMcAIiA/Pz8AkorlAVMA2s8A5tpiGnWFJJ4AxrwAqqEAFRQ7ABWnp6cALTsdCCJOFV0AICoATGOoLsnJAUlwACgAXXkAaooAdpoAUGggCCYsABBQAB0UAAcAM0IAT0sAW1YSBRajJrDCADlhACMAamW7AESJADK0MNYvDTgAqp4APDqMJqgNAw9SFmKvAD87D0YAfHYWAAAAMCwoODV1H4vhaIld1M1kHYE1AAB0AB0rCzOLACltFWm7k5x6kI/XNGuDNq8SPVQwb68aUXPNjedpAAAJRElEQVR4nO2d+UMayx3Al1vCoaIsiyiXHIrcoEkwCqhovNJqNH0k0Vr68p7vvba2//8v3Z1ddmd2Z3Ug5Qj9fn6S2YEZPpn7IJwVGB4OAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAXKFk9KiuTzswPR9DrVlhbhosMg7JoUfFuTTozPxxg73sIgr2BeMhdY69we6+w8JuP487XD8F1la/9pL18hdkLasFXcXvldPyZm3KKEZ638TtFNYBub71it9vjq+uTyOL0cpHjbRJlNYRu76sd0X47iUxOK/c1m8JFP4hq78CuUDmYTEankKJS8ET4WkgJpNk7rdhVku8nld3p4qKqyhP15RR9FHvrbTtGGxo/icuaDUdp+ij2krg8exzsIe553F7tEgUa7Z0S8uzQ8im8xvXx1QcpzGDvfZyQtzrRHE8Tb6qEvtdSmN7e2UdCXhs6DRWy6ePvOaO9G7LRg/kGRpnoOKoX5DxXtHdK1lto9AhyRN3dedCVvbMKIS856exOGQ9k0xfhtjR7lsX+DA0aPROuiWGL7ZKw9yvZ6F0NmUZoTsIQjEJDlPim6N8hf7ApA332cETIUd8/MHt/kPV26MGKx+L1ei0LutB5FLpoiL0SDIgPvL68wbcfvQNbddxye58hq0/xf8F5cx9/WcwRhe9ntypv+Z+EPHJ59OTLHXOKHvRxRnsSPl2oP62mH9DvSvlRMLbquOW1PIN7BPZCGWeh09zQAi6IYUtEy5H1N1xeBZuhfah3W45N5iQHsOfJ4t+/MU88lO3hZe9Ze8sjsLcbczqdsULmXA0hmr4Il1AST3CreKOnDVb2NqMph8MRfceaJLs9j05Hmqi9U2DvOOyUiXXOlSqMz9giHLeM0natEPbkRu/zyV7XoZCqsybJbK9kMNDAHxvtrY3bXsbZJxyOdQ53xaDiDo/b80v/ost5Drf38UyMd1fvpgShb0/osibJbM+FgryBhC/tUopVHntssOcPLqoE0RuyeMgIzkI4ccLOQuZwH5+xifY4n8WC+kLNXvwX7rG3GXWo6iRaj4xJstqTR+peH/rWWwG5CmDPDfYI3ProI+Ap7NQTKzSlvSHN3pJLbPQ43N7Nt5ZOnVR19xjTZLXnIkL8cgeCdRyTt1cwyENF8Lhcrdl4xR63ImdCthev/Pa7wRyqutuMaTLaK2XJ7y/3CdgAZeL2NmI0e2L5E0cukZ0az0ewyKK9Sjt5yt1FKe6kXpcxUUZ71jVdNNR9JbTXE7d3TJfnDGfEh8XLcg63d9NevRJ7i3ebdHkOgbHhY7QnlzWsqUf2AtrridvLGJs9hUP0vPiARV6XB8jfTOQ5hB5booz2fChgSQuYNnv7GZOaK9bdpsl79mhtnuTOkWqxpTor9riNZqYQo5a/cGGD+o5HeqMnpFrdL4yJzow9kf3mUcFJERjuUKPTGj3B0ep9YZ6ozZQ9kY3zQ1oNptXdHq2v7X1iV8fNnD2J/aNCDC+CMXHeS4mmLAmohS4a/fZh0KRm0J7I+VFGbgPD0qKVWazP0nKU1HMIQnSzx76qpzGb9lAVzoj9xRG5YGrg8ct2VEh168Oo42bXnsjjt3/dnb0Y683Tv//zvOFnmFF7J3s9sVKKNXK7/ty0Yfe4EwuHw87M4RN9VPMCM2lvr9tKCf3e4LZbp/ej+4cZdYATjmU6T4MnNHv27rpiX0rMIlJRyppJ6IjomMW/Y4XO7oBpzZa9k71uymFASH2mxG3SBoaxo/NBGsEZsndXF/tP6tSVPvE6pE7rwoXOMXMRnBV7H+qbt9SVThGzbQqTZRm0qM+W6szYa5mocwi3JybvMVtQlRYW2FKdFXtc10SeI4pGwu9P8QN6P12iWzBPpvZM5yckw9rLTpu9PUp3gYpenePWD1bbxCGzMr9TvhYFHpnpYxz90e3NoU0zbMPWYK+B7Gkb4oaTGATj6DVMVuu63NXXdlw9oid/0zLP87Wd15efM3R5GcY00a3VtbwudA5toSXUPbM5dILFjZ0eQDqX/eprdL7L6zFJZRz2tikNn+D4U7ISxw44Wr0oy2Vpk4231aqRTJi2IMjYaXBot8dl2JtukEUyj7Y1sE0gLm8hS6fcDvo5OuOw986wKxtt3XF/Jo6H+rOWNem7lvubvPxfuKZxTT9GW9EyMk9u02rIFdqt2CjJB0CIc1MopH9tZClh0eslGMt4D2/4hNTtZv0DceRCtBeSsplYwuzZbPfklA0VvQxDsxdascpnAiiHShSty8FSKFR6Je99B0p4DLkltDTyc9y8RzmmYVZxx2NPrbqCcLtd/yQF4Ue7k/08Bwl7VXQNcPe4U9Dq8BFDaomAch4lTXnYPwIVcLlkxRbdYciSch4tK0ZQDvyYFr3x2OtvkUV7d/1lAfxcfFLp2SzePG6PzylXeDfOO7EBKm7/LGWA+rRh0aF3Y9UfknK/kNSo7Z3cCo7o7fYnLYS4z5L8W1bNaBm/Bohtk+92pEV9pqGyYi9rPLWMSJNujN99y01EyJYoH4InNfJVgl5rm1gjJu+zJBNqVtNl4jw9/usFXLNTYKm4ir206Vkw37KmxtugOPYEMHkJs/5WTWrk9t6R63in5NHuv2Pf5mfyHluReN8G01BZqnmuhSXzCHm1+CUWqGfcS75+8ct65mkR+njNW4iRQV6+tbexGwdeH3kXJjfEx6cbnpUXDv7PLywmEr4FczOhfDCdaFhLJrVfTSohYhwXjRTy8m3lr8R9DfIuDPbrBQDigJAXP9DdFbqmXeEFFN4a7qGR9vRXeCed36lC3+hxBnv4gfAhm76ZZZWQF5d+bUVnT//rBfeTzvL0cEVevr2RwvT2yCu8fPXi+Y/8P4Js9L6iMIM93RXe3JtJ5nh6ONM1evLlW6M9Tn+FFxA5IOpt//ItxZ7u1wug8Em8JfqMGyWUYo+LYO5qZbAng/Ua6uVbmr036rCFr8KAWWW9vzhVUX9xgGaPK9aQPt72ejL5nFZu4nijx5nY467lggejPR1XbTvxSyF0e9KvF/A7MNYzsP4VzdD6mNh72OFhhYXKDX7w1sQedwkFjwEzewALYO97eAX2vgNPwKUC/8fBwIQ0Jp0VAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC+EyswPP8Fkn8Vr2AT/kcAAAAASUVORK5CYII=\">\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/components/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Login</h2>\n<form (submit)=\"onLoginSubmit()\">\n  <div class=\"form-group\">\n    <label>Username</label>\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"username\" name=\"username\">\n  </div>\n  <div class=\"form-group\">\n    <label>Password</label>\n    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"password\" name=\"password\">\n  </div>\n  <input type=\"submit\" class=\"btn btn-primary\" value=\"Login\">\n</form>"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = (function () {
    function LoginComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        };
        //Auth service
        this.authService.authenticateUser(user).subscribe(function (data) {
            if (data.success) {
                _this.authService.storeUserData(data.token, data.user);
                _this.authService.getProfile().subscribe(function (profile) {
                    _this.isBanned = profile.user.isBanned;
                    console.log(_this.isBanned);
                    if (_this.isBanned) {
                        _this.flashMessage.show('Your are banned on this website', { cssClass: 'alert-danger', timeout: 5000 });
                        _this.router.navigate(['profile']);
                    }
                    else {
                        _this.flashMessage.show('Your login correctly', { cssClass: 'alert-success', timeout: 5000 });
                        _this.router.navigate(['profile']);
                    }
                });
            }
            else {
                _this.flashMessage.show(data.message, { cssClass: 'alert-danger', timeout: 5000 });
                _this.router.navigate(['login']);
            }
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".navbar {\r\n    position: relative; \r\n    top: -25px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">\n    <a class=\"navbar-brand\" [routerLink]=\"['/']\">AlexBlogApp</a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n\n    <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\n      <ul class=\"navbar-nav mr-auto\">\n        <li  [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" class=\"nav-item\">\n          <a class=\"nav-link\" [routerLink]=\"['/']\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i> Home</a>\n        </li>\n      </ul>\n      <ul class=\"navbar-nav navbar-right\">\n          <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" class=\"nav-item\">\n            <a class=\"nav-link\" [routerLink]=\"['/login']\"><i class=\"fa fa-address-book\" aria-hidden=\"true\"></i> Login</a>\n          </li>\n          <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" class=\"nav-item\">\n            <a class=\"nav-link\" [routerLink]=\"['/register']\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i> Register</a>\n          </li>\n          <li [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" class=\"nav-item\">\n            <a class=\"nav-link\" [routerLink]=\"['/blog']\"><i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i> Blog</a>\n          </li>\n          <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" class=\"nav-item\">\n            <a class=\"nav-link\" [routerLink]=\"['/profile']\"><i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profile</a>\n          </li>\n          <li *ngIf=\"authService.loggedIn()\" class=\"nav-item\">\n            <a class=\"nav-link\" href=\"#\" (click)=\"onLogoutClick()\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i> Logout</a>\n          </li>\n      </ul>\n      </div>\n  </nav>"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = (function () {
    function NavbarComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessage.show('You are logout', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/login']);
        return false;
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".main-section{\r\n\tborder:1px solid #138496;\r\n\tbackground-color: #fff;\r\n}\r\n.profile-header{\r\n\tbackground-color: #17a2b8;\r\n\theight:150px;\r\n}\r\n.user-detail{\r\n\tmargin:-50px 0px 30px 0px;\r\n}\r\n.user-detail img{\r\n\theight:100px;\r\n\twidth:100px;\r\n}\r\n.user-detail h5{\r\n\tmargin:15px 0px 5px 0px;\r\n}\r\n.user-social-detail{\r\n\tpadding:15px 0px;\r\n\tbackground-color: #17a2b8;\r\n}\r\n.user-social-detail a i{\r\n\tcolor:#fff;\r\n\tfont-size:23px;\r\n\tpadding: 0px 5px;\r\n}\r\n\r\n.user-list {\r\n\tpadding-top: 25px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf = \"user.isBanned\">\n    <h3 class=\"text-danger text-center\">You are banned man</h3>\n</div>\n\n<div *ngIf = \"user.role == 'admin'\">\n    <h3 class=\"text-primary text-center\">You are admin of this Website</h3>\n</div>\n  \n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"offset-lg-4 col-lg-4 col-sm-6 col-12 main-section text-center\">\n          <div class=\"row\">\n              <div class=\"col-lg-12 col-sm-12 col-12 profile-header\"></div>\n          </div>\n          <div class=\"row user-detail\">\n              <div class=\"col-lg-12 col-sm-12 col-12\">\n                  <img class=\"rounded-circle img-thumbnail\" src=\"{{user.avatar}}\" alt=\"{{user.avatar}}\">\n                  <p>{{user.avatar}}</p>\n                  <h5>{{user.username}}</h5>\n                  <p><i class=\"fa fa-envelope\" aria-hidden=\"true\"></i> {{user.email}}</p>\n\n                  <hr>\n                  <span>Welcome in my app</span>\n              </div>\n          </div>\n          <div class=\"row user-social-detail\">\n              <div class=\"col-lg-12 col-sm-12 col-12\">\n                  <a target=\"_blank\" href=\"https://bitbucket.org/hollowdadon/blog\"><i class=\"fa fa-bitbucket\" aria-hidden=\"true\"></i></a>\n                  <a target=\"_blank\"  href=\"https://hollowdadon-alex-store.herokuapp.com/\"><i class=\"fa fa-cart-plus\" aria-hidden=\"true\"></i></a>\n                  <a target=\"_blank\"  href=\"https://twitter.com/nodejs?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a>\n              </div>\n          </div>\n      </div>\n    </div>\n\n    <div class=\"row user-list\" *ngIf = \"user.role == 'admin'\">\n        <h3>List of the Users</h3>\n        <ul *ngFor=\"let user of allUsers\">\n            <a [routerLink]=\"['/user/', user.username]\">{{user.username}}</a>\n        </ul>\n    </div>\n\n  </div>\n\n<div class=\"container\">\n    <input id=\"photo\" type=\"file\" />\n    <!-- button to trigger the file upload when submitted -->\n    <button type=\"button\" class=\"btn btn-success\" (click)=\"upload()\">\n    UPLOAD IMAGE\n    </button>\n</div>\n\n\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { Component, OnInit } from '@angular/core';

//import the native angular http and respone libraries

//import the do function to be used with the http library.

//import the map function to be used with the http library





var ProfileComponent = (function () {
    //public uploader:FileUploader = new FileUploader({url: 'http://localhost:3000/users/profile', itemAlias: 'photo'});
    function ProfileComponent(http, el, authService, router, formBuilder, flashMessage) {
        this.http = http;
        this.el = el;
        this.authService = authService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.flashMessage = flashMessage;
        this.user = {};
    }
    ProfileComponent.prototype.displayAllUsers = function () {
        var _this = this;
        this.authService.getAllUsers().subscribe(function (data) {
            _this.allUsers = data.users;
        });
    };
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            _this.user = profile.user;
            console.log(_this.user.avatar);
            if (_this.user.role == 'admin') {
                _this.displayAllUsers();
            }
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    ProfileComponent.prototype.upload = function () {
        var _this = this;
        //locate the file element meant for the file upload.
        var inputEl = this.el.nativeElement.querySelector('#photo');
        //get the total amount of files attached to the file input.
        var fileCount = inputEl.files.length;
        //create a new fromdata instance
        var formData = new FormData();
        //check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) {
            //append the key name 'photo' with the first file in the element
            formData.append('photo', inputEl.files.item(0));
            //call the angular http method
            this.http
                .put('users/profile', formData).map(function (res) { return res.json(); }).subscribe(
            //map the success function and alert the response
            function (success) {
                _this.flashMessage.show('Image added', { cssClass: 'alert-success', timeout: 5000 });
                window.location.reload();
                //this.router.navigate(['blog']);
            }, function (error) { return alert(error); });
        }
    };
    ProfileComponent.prototype.uploadImageByLink = function () {
        var _this = this;
        this.authService.uploadImage().subscribe(function (data) {
            _this.flashMessage.show('Image uploaded', { cssClass: 'alert-success', timeout: 5000 });
            window.location.reload();
        });
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__("../../../../../src/app/components/profile/profile.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__["FlashMessagesService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\">Register</h2>\n<form (submit)=\"onRegisterSubmit()\">\n  <div class=\"form-group\">\n    <label>Username</label>\n    <input type=\"text\"  [(ngModel)]=\"username\" name=\"username\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <label>Email</label>\n    <input type=\"text\" [(ngModel)]=\"email\" name=\"email\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <label>Password</label>\n    <input type=\"password\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\">\n  </div>\n  <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">\n</form>"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = (function () {
    function RegisterComponent(validateService, flashMessage, authService, router) {
        this.validateService = validateService;
        this.flashMessage = flashMessage;
        this.authService = authService;
        this.router = router;
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            email: this.email,
            password: this.password,
            validation: this.validation
        };
        //Required Fiedls
        if (!this.validateService.validateRegister(user)) {
            this.flashMessage.show('Plese fill in all fields', { cssClass: 'alert-danger flash-message', timeout: 5000 });
            return false;
        }
        //ValidateEmil
        if (!this.validateService.validateEmail(user.email)) {
            this.flashMessage.show('Type email correctly', { cssClass: 'alert-danger', timeout: 5000 });
            return false;
        }
        //Register User
        this.authService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                _this.flashMessage.show('You are now registered you can log in now', { cssClass: 'alert-success', timeout: 5000 });
                _this.router.navigate(['/login']);
            }
            else {
                _this.flashMessage.show(data.message, { cssClass: 'alert-danger', timeout: 5000 });
                _this.router.navigate(['/register']);
            }
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_validate_service__["a" /* ValidateService */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/update-post/update-post.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/update-post/update-post.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Edit Blog</h1>\n\n<div class=\"row show-hide-message\" *ngIf=\"message\">\n    <div [ngClass]=\"messageClass\">\n      {{ message }}\n    </div>\n  </div>\n\n<form (submit)=\"updateBlogSubmit()\" >\n  <div class=\"form-group\">\n    <label for=\"title\">Title</label>\n    <input [disabled]=\"processing\" type=\"text\" name=\"title\" placeholder=\"*Title\" class=\"form-control\"  [(ngModel)]=\"blog.title\">\n  </div>\n\n  <div class=\"form-group\">\n      <label for=\"body\">Body</label>\n      <textarea [disabled]=\"processing\" name=\"body\" rows=\"8\" cols=\"80\" [(ngModel)]=\"blog.body\" class=\"form-control\" placeholder=\"*Body\"></textarea>\n    </div>\n\n    <a [routerLink]=\"['/delete-blog/', blog._id]\"><button [disabled]=\"processing\"\n       type=\"button\" name=\"button\" class=\"btn btn-danger\">Delete</button></a>\n       <button type=\"button\" name=\"button\" class=\"btn btn-warning\" (click)=\"goBack()\">Go Back</button>\n       <button [disabled]=\"processing\" type=\"submit\" name=\"save\" class=\"btn btn-success\">Save Changes</button>\n\n</form>"

/***/ }),

/***/ "../../../../../src/app/components/update-post/update-post.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdatePostComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_blog_service__ = __webpack_require__("../../../../../src/app/services/blog.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UpdatePostComponent = (function () {
    function UpdatePostComponent(location, activatedRoute, blogService, router) {
        this.location = location;
        this.activatedRoute = activatedRoute;
        this.blogService = blogService;
        this.router = router;
        this.blog = {
            title: String,
            body: String,
            _id: String
        };
        this.processing = false;
        this.loading = true;
    }
    UpdatePostComponent.prototype.updateBlogSubmit = function () {
        var _this = this;
        this.processing = true;
        this.blogService.editBlog(this.blog).subscribe(function (data) {
            if (!data.success) {
                _this.messageClass = 'alert alert-danger';
                _this.message = data.message;
                _this.processing = false;
            }
            else {
                _this.messageClass = 'alert alert-success';
                _this.message = data.message;
                setTimeout(function () {
                    _this.router.navigate(['/blog']);
                }, 1000);
            }
        });
    };
    UpdatePostComponent.prototype.goBack = function () {
        this.location.back();
    };
    UpdatePostComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentUrl = this.activatedRoute.snapshot.params;
        this.blogService.getSingleBlog(this.currentUrl.id).subscribe(function (data) {
            if (!data.success) {
                _this.messageClass = 'alert alert-danger';
                _this.message = 'Blog not found';
            }
            else {
                _this.blog = data.blog;
                _this.loading = false;
            }
        });
    };
    UpdatePostComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-update-post',
            template: __webpack_require__("../../../../../src/app/components/update-post/update-post.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/update-post/update-post.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__services_blog_service__["a" /* BlogService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], UpdatePostComponent);
    return UpdatePostComponent;
}());



/***/ }),

/***/ "../../../../../src/app/guards/admin.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminGuard = (function () {
    function AdminGuard(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    AdminGuard.prototype.canActivate = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            _this.role = profile.user.role;
        });
        if (this.role == "user") {
            //return true;
            this.flashMessage.show('Your are not admin on this website', { cssClass: 'alert-danger', timeout: 1000 });
            this.router.navigate(['/blog']);
            return false;
        }
        else {
            return true;
        }
    };
    AdminGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], AdminGuard);
    return AdminGuard;
}());



/***/ }),

/***/ "../../../../../src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "../../../../../src/app/guards/banned.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BannedGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BannedGuard = (function () {
    function BannedGuard(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    BannedGuard.prototype.canActivate = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            _this.isBanned = profile.user.isBanned;
        });
        console.log(this.isBanned + " Banned guard");
        if (!this.isBanned) {
            return true;
        }
        else {
            this.flashMessage.show('Your are banned on this website', { cssClass: 'alert-danger', timeout: 1000 });
            this.router.navigate(['/profile']);
            return false;
        }
    };
    BannedGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], BannedGuard);
    return BannedGuard;
}());



/***/ }),

/***/ "../../../../../src/app/guards/logged-in.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoggedInGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoggedInGuard = (function () {
    function LoggedInGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    LoggedInGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            this.router.navigate(['/profile']);
            return true;
        }
        else {
            return true;
        }
    };
    LoggedInGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], LoggedInGuard);
    return LoggedInGuard;
}());



/***/ }),

/***/ "../../../../../src/app/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
    }
    AuthService.prototype.registerUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('users/register', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.authenticateUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('users/authenticate', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.getProfile = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('users/profile', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.getAcessibleProfile = function (username) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('users/acessibleProfile/' + username, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    };
    AuthService.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token');
        this.authToken = token;
    };
    AuthService.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__["tokenNotExpired"])('id_token');
    };
    AuthService.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService.prototype.banUser = function (username) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.put('users/acessibleProfileBan/' + username, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.unbanUser = function (username) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.put('users/acessibleProfileUnban/' + username, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.uploadImage = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.put('users/profile/', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.getAllUsers = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('users/allUsers/', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "../../../../../src/app/services/blog.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlogService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BlogService = (function () {
    function BlogService(authService, http) {
        this.authService = authService;
        this.http = http;
    }
    BlogService.prototype.createAuthenticationHeaders = function () {
        this.authService.loadToken();
        this.options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["RequestOptions"]({
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["Headers"]({
                'Content-Type': 'application/json',
                'authorization': this.authService.authToken
            })
        });
    };
    BlogService.prototype.newBlog = function (blog) {
        this.createAuthenticationHeaders();
        return this.http.post('blogs/newBlog', blog, this.options)
            .map(function (res) { return res.json(); });
    };
    BlogService.prototype.getAllBlogs = function () {
        this.createAuthenticationHeaders();
        return this.http.get('blogs/allBlogs', this.options)
            .map(function (res) { return res.json(); });
    };
    BlogService.prototype.getSingleBlog = function (id) {
        this.createAuthenticationHeaders();
        return this.http.get('blogs/singleBlog/' + id, this.options)
            .map(function (res) { return res.json(); });
    };
    BlogService.prototype.editBlog = function (blog) {
        this.createAuthenticationHeaders(); // Create headers
        return this.http.put('blogs/updateBlog/', blog, this.options).map(function (res) { return res.json(); });
    };
    BlogService.prototype.deleteBlog = function (id) {
        this.createAuthenticationHeaders(); // Create headers
        return this.http.delete('blogs/deleteBlog/' + id, this.options).map(function (res) { return res.json(); });
    };
    BlogService.prototype.likeBlog = function (id) {
        var info = { id: id };
        return this.http.put('blogs/likeBlog/', info, this.options).map(function (res) { return res.json(); });
    };
    BlogService.prototype.dislikeBlog = function (id) {
        var info = { id: id };
        return this.http.put('blogs/dislikeBlog/', info, this.options).map(function (res) { return res.json(); });
    };
    BlogService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]])
    ], BlogService);
    return BlogService;
}());



/***/ }),

/***/ "../../../../../src/app/services/validate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidateService = (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateRegister = function (user) {
        if (user.username == undefined || user.email == undefined || user.password == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    ValidateService.prototype.validateTitle = function (title) {
        if (title.length < 5 || title.length > 20) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateBody = function (body) {
        if (body.length < 5 || body.length > 20) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map